import requests
import tkinter as tk
from PIL import ImageTk, Image
from random import randint
import re


# imgURL = 'http://www.pngall.com/wp-content/uploads/2/Fireball-PNG-Image-File.png'
# icon = requests.get(imgURL).content
# with open('merel.png', 'wb') as f:
#    f.write(img)


def getRandom():
    rand = randint(0, 19)
    randLang = data[rand]['name']
    return getData(randLang, data)


def getMore(listbox, tabData):
    selected = listbox.curselection()[0]
    selectedLang = tabData[selected]["name"]
    return getData(selectedLang, tabData)


def getData(value, tabData):
    results = tk.Label(frame5, width=40, height=22,
                       font="Gadugi", justify="left", fg=color)
    results.grid(row=3, column=1, columnspan=4)
    if value == '':
        results["text"] = 'Type something.'
    else:
        results["text"] = getOne(value, tabData)


def getAll(tabData):
    results = tk.Label(frame5, width=65, height=34)
    results.grid(row=3, column=1, columnspan=4)

    listbox = tk.Listbox(frame5, width=20, height=20,
                         font="Calibri", fg="white", bg=color, relief="flat", selectbackground=color2)
    for i in range(len(tabData)):
        listbox.insert(i, tabData[i]["name"])
    listbox.grid(row=3, column=2)

    moreBtn = tk.Button(frame5, relief="flat", font="Verdana", text="MORE", fg=color, activebackground=color2,
                        command=lambda: getMore(listbox, tabData))
    moreBtn.grid(row=3, column=3)


def getOne(thisLang, tabData):
    hints = []
    for lang in tabData:
        if lang['name'].lower() == thisLang.lower():
            name = lang['name']
            frameworks = lang['frameworks']
            code = lang['code']
            return 'Name: %s \nFrameworks: %s \nLittle code: \n%s' % (name, frameworks, code)
    for lang in data:
        pattern = '[' + thisLang[0].upper() + ']' + '['
        for i in range(1, len(thisLang)):
            pattern += thisLang[i].lower()
        pattern += ']*'
        exist = re.search(pattern, lang['name'])
        if exist:
            hints.append(lang)
    if hints:
        return getAll(hints)
    else:
        return 'No results'


root = tk.Tk()

languages = requests.get('https://lobster22.github.io/data/lang.json')
data = languages.json()

_width = 480
color = "#AD2450"
color2 = "#ff125e"

root.title('Codepedia')
root.iconbitmap('./icon.ico')

# Images for buttons
codeImg = ImageTk.PhotoImage(Image.open('./code.png'))
diceImg = ImageTk.PhotoImage(Image.open('./dice.png'))
maginierImg = ImageTk.PhotoImage(Image.open('./magnifier.png'))

# Elements
frame1 = tk.Frame(root, width=_width, height=10)
frame1.grid(row=0, column=0, columnspan=6)

frame2 = tk.Frame(root, width=50, height=30)
frame2.grid(row=1, column=0)

codeBtn = tk.Button(root, width=70, height=50, relief="flat",
                    image=codeImg, activebackground=color2, command=lambda: getAll(data))
codeBtn.grid(row=1, column=1)

param = tk.Entry(root, width=15, fg=color2, font="Verdana")
param.grid(row=1, column=2)

searchBtn = tk.Button(root, width=20, height=20, relief="flat",
                      image=maginierImg, activebackground=color2, command=lambda: getData(param.get(), data))
searchBtn.grid(row=1, column=3)

randBtn = tk.Button(root, width=20, height=20,
                    relief="flat", image=diceImg, activebackground=color2, command=getRandom)
randBtn.grid(row=1, column=4)

frame3 = tk.Frame(root, width=50, height=30)
frame3.grid(row=1, column=5)

frame4 = tk.Frame(root, width=_width, height=5, bg=color)
frame4.grid(row=2, column=0, columnspan=6)

frame5 = tk.Frame(root, width=_width, height=520)
frame5.grid(row=3, column=0, columnspan=6)

results = tk.Label(frame5, width=65, height=34)
results.grid(row=3, column=1, columnspan=4)

root.mainloop()
